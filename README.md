# README #

Notes and Observations about the task:

The name generator should work for any combination of uppercase and lowercase letters, as long as the user keeps the initial format ("Lastname, Firstname")

The program traces the steps of the ninja if the tracestep variable is turned to true. This was implemented in order to increase the chance of the ninja finding and successfully destroying the shire of the Red Claw clan. Turning this function on also means that the ninja cannot step  on the same coordinate twice. 

Bugs:

- The program doesn't print the current map after a direction modifier(N,W,S,E). This is due to the fact that a loop inside the Move function handles the moving when modifiers are involved, meanwhile, the program only prints the map in the main function.

- Direction modifiers don't work properly in some cases.

- Due to a misunderstanding of the task on my part, the ninja checks possible directions in every step and changes accordingly, which causes an increased number of infinite loops.




.